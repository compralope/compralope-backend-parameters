#!/bin/bash

git pull
git checkout .
git checkout $1
git pull
mvn clean package
docker rm -f ms_parameters
docker rmi ms_parameters
docker build -t ms_parameters .
docker run -p 8086:8086 --name ms_parameters --link RBC:database -d ms_parameters
docker ps -a

exit 0