INSERT INTO dbo.tenant
(
    title,
    business_name,
    ruc,
    details,
    registration_date,
    registration_update,
    active
)
VALUES
(   'Compralope',        -- title - varchar(256)
    'Compralope S.A.C.',        -- business_name - varchar(256)
    '20123456789',        -- ruc - varchar(256)
    N'',       -- details - nvarchar(1024)
    GETDATE(), -- registration_date - datetime
    GETDATE(), -- registration_update - datetime
    1       -- active - bit
    );

INSERT INTO dbo.entity
(
    title,
    details,
    active
)
VALUES
(   'Products',  -- title - varchar(128)
    N'Tabla de productos del tenant', -- details - nvarchar(1024)
    1 -- active - bit
    );

INSERT INTO dbo.state
(
    entity_id,
    title,
    details,
    order_index,
    active
)
VALUES
(   1,   -- entity_id - int
    'Activo',  -- title - varchar(256)
    N'Estado activo del producto', -- details - ntext
    1,   -- order_index - int
    1 -- active - bit
    );

INSERT INTO dbo.state
(
    entity_id,
    title,
    details,
    order_index,
    active
)
VALUES
(   1,   -- entity_id - int
    'Inactivo',  -- title - varchar(256)
    N'Estado activo del producto', -- details - ntext
    2,   -- order_index - int
    1 -- active - bit
    );