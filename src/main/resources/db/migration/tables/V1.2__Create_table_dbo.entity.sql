CREATE TABLE dbo.entity
( 
	id                   int IDENTITY ( 1,1 ) ,
	title                varchar(128)  NULL ,
	details              nvarchar(1024)  NULL ,
	active               bit  NULL 
)
go



ALTER TABLE dbo.entity
	ADD CONSTRAINT PK_entity_id PRIMARY KEY (id ASC)
go