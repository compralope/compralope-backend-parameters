
CREATE TABLE dbo.ubigeo
( 
	id                   int IDENTITY ( 1,1 ) ,
	country_code         varchar(8)  NULL ,
	country_title        varchar(256)  NULL ,
	department_code      varchar(8)  NULL ,
	department_title    varchar(256)  NULL ,
	province_code        varchar(8)  NULL ,
	province_title       varchar(256)  NULL ,
	district_code        varchar(8)  NULL ,
	district_title       varchar(256)  NULL ,
	zip_code             varchar(8)  NULL ,
	active               bit  NULL 
)
go



ALTER TABLE dbo.ubigeo
	ADD CONSTRAINT PK_ubigeo_id PRIMARY KEY (id ASC)
go



CREATE INDEX IK_ubigeo_country_code ON dbo.ubigeo
( 
	country_code          ASC
)
go



CREATE INDEX IK_ubigeo_province_code ON dbo.ubigeo
( 
	province_code         ASC
)
go



CREATE INDEX IK_ubigeo_district_code ON dbo.ubigeo
( 
	district_code         ASC
)
go



CREATE INDEX IK_ubigeo_active ON dbo.ubigeo
( 
	active                ASC
)
go
