CREATE TABLE dbo.state
( 
	id                   int IDENTITY ( 1,1 ) ,
	entity_id            int  NULL ,
	title                varchar(256)  NULL ,
	details              ntext  NULL ,
	order_index          int  NULL ,
	active               bit  NULL 
)
go

ALTER TABLE dbo.state
	ADD CONSTRAINT PK_state_id PRIMARY KEY (id ASC)
go

ALTER TABLE dbo.state
	ADD CONSTRAINT FK_state_entity_id FOREIGN KEY (entity_id) REFERENCES dbo.entity(id)
go