
CREATE TABLE dbo.media
( 
	id                   int IDENTITY ( 1,1 ) ,
	tenant_id            int  NULL ,
	code                 varchar(64)  NULL ,
	title                varchar(256)  NULL ,
	url                  varchar(512)  NULL ,
	content_type         varchar(32)  NULL 
)
go

ALTER TABLE dbo.media
	ADD CONSTRAINT PK_media_id PRIMARY KEY (id ASC)
go


ALTER TABLE dbo.media
	ADD CONSTRAINT FK_media_tenant_id FOREIGN KEY (tenant_id) REFERENCES dbo.tenant(id)
go