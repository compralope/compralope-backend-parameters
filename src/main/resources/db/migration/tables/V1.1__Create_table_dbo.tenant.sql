CREATE TABLE dbo.tenant
( 
	id                   int IDENTITY ( 1,1 ) ,
	title                varchar(256)  NULL ,
	business_name        varchar(256)  NULL ,
	ruc                  varchar(256)  NULL ,
	details              nvarchar(1024)  NULL ,
	registration_date    datetime  NULL ,
	registration_update  datetime  NULL ,
	active               bit  NULL 
)
go



ALTER TABLE dbo.tenant
	ADD CONSTRAINT PK_tenant_id PRIMARY KEY (id ASC)
go