package pe.compralo.parameters.application.mapper;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;

import pe.compralo.parameters.api.request.StateRequest;
import pe.compralo.parameters.api.response.StateResponse;
import pe.compralo.parameters.application.domain.State;

public class StateMapper {

	public static StateResponse ToSelectStateResponse(State i) {
		StateResponse o = new StateResponse();

		BeanUtils.copyProperties(i, o);

		return o;
	}

	public static List<StateResponse> ToArraySelectStateResponse(List<State> l) {
		List<StateResponse> o = new ArrayList<StateResponse>();

		for(State i:l)
			o.add(StateMapper.ToSelectStateResponse(i));

		return o;
	}

	public static StateResponse ToListStateResponse(State i) {
		StateResponse o = new StateResponse();

		BeanUtils.copyProperties(i, o);

		return o;
	}

	public static List<StateResponse> ToArrayListStateResponse(List<State> l) {
		List<StateResponse> o = new ArrayList<StateResponse>();

		for(State i:l)
			o.add(StateMapper.ToListStateResponse(i));

		return o;
	}

	public static State FromStateRequest(StateRequest i) {
		State o = new State();

		BeanUtils.copyProperties(i, o);

		return o;
	}

	public static StateResponse ToRegisterStateResponse(State i) {
		StateResponse o = new StateResponse();

		BeanUtils.copyProperties(i, o);

		return o;
	}

}