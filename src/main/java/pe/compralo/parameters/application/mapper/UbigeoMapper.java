package pe.compralo.parameters.application.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import pe.compralo.parameters.api.response.UbigeoResponse;
import pe.compralo.parameters.application.domain.Ubigeo;

public class UbigeoMapper {

	public static UbigeoResponse ToSelectUbigeoResponse(Ubigeo i) {
		UbigeoResponse o = new UbigeoResponse();

		BeanUtils.copyProperties(i, o);

		return o;
	}

	public static List<UbigeoResponse> ToArraySelectUbigeoResponse(List<Ubigeo> l) {
		List<UbigeoResponse> o = new ArrayList<UbigeoResponse>();

		for(Ubigeo i:l)
			o.add(UbigeoMapper.ToSelectUbigeoResponse(i));

		return o;
	}

}