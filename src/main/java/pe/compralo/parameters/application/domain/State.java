package pe.compralo.parameters.application.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class State
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "entity_id",nullable = false)
	private int entityId;

	@Column(name = "title",nullable = false)
	private String title;

	@Column(name = "details")
	private String details;

	@Column(name = "order_index")
	private int orderIndex;

	@Column(name = "active")
	private Boolean active;
}