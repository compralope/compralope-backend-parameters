package pe.compralo.parameters.application.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Ubigeo
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "country_code",nullable = false)
	private String countryCode;

	@Column(name = "country_title",nullable = false)
	private String countryTitle;

	@Column(name = "department_code",nullable = false)
	private String departmentCode;

	@Column(name = "department_title",nullable = false)
	private String departmentTitle;

	@Column(name = "province_code",nullable = false)
	private String provinceCode;

	@Column(name = "province_title",nullable = false)
	private String provinceTitle;

	@Column(name = "district_code",nullable = false)
	private String districtCode;

	@Column(name = "district_title",nullable = false)
	private String districtTitle;

	@Column(name = "zip_code",nullable = false)
	private String zipCode;

	@Column(name = "active",nullable = false)
	private Boolean active;

}