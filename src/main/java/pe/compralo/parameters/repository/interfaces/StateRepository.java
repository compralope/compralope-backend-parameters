package pe.compralo.parameters.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.compralo.parameters.application.domain.State;

import java.util.List;

public interface StateRepository extends JpaRepository<State, Integer> {
    List<State> findByEntityIdAndActiveOrderByOrderIndexAsc(int entityId,Boolean active);
}
