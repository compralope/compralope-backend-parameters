package pe.compralo.parameters.repository.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.compralo.parameters.application.domain.Ubigeo;
import pe.compralo.parameters.cross.utils.KeyValuePair;


public interface UbigeoRepository extends JpaRepository<Ubigeo, Integer> {

    @Query(value = "select distinct NEW pe.compralo.parameters.cross.utils.KeyValuePair(u.countryCode,u.countryTitle) from Ubigeo u " +
                    "where u.active = 1 order by u.countryTitle")
    List<KeyValuePair<String,String>> findCountry();

    @Query(value = "select distinct NEW pe.compralo.parameters.cross.utils.KeyValuePair(u.departmentCode,u.departmentTitle) from Ubigeo u " +
                    "where u.countryCode = :countryCode " +
                    "and u.active = 1 order by u.departmentTitle")
    List<KeyValuePair<String,String>> findDepartmentByCountryCode(@Param("countryCode")String countryCode);

    @Query(value = "select distinct NEW pe.compralo.parameters.cross.utils.KeyValuePair(u.provinceCode,u.provinceTitle) from Ubigeo u " +
            "where u.countryCode = :countryCode " +
            "and u.departmentCode = :departmentCode " +
            "and u.active = 1 order by u.provinceTitle")
    List<KeyValuePair<String,String>> findProvinceByCountryCodeAndDepartmentCode(@Param("countryCode")String countryCode,@Param("departmentCode")String departmentCode);

    @Query(value = "select distinct NEW pe.compralo.parameters.cross.utils.KeyValuePair(u.districtCode,u.districtTitle) from Ubigeo u " +
            "where u.countryCode = :countryCode " +
            "and u.departmentCode = :departmentCode " +
            "and u.provinceCode = :provinceCode " +
            "and u.active = 1 order by u.districtTitle")
    List<KeyValuePair<String,String>> findDistrictByCountryCodeAndDepartmentCodeAndProvinceCode(@Param("countryCode")String countryCode,@Param("departmentCode")String departmentCode,@Param("provinceCode")String provinceCode);

    @Query(value = "select NEW pe.compralo.parameters.cross.utils.KeyValuePair(u.id,u.zipCode) from Ubigeo u " +
            "where u.countryCode = :countryCode " +
            "and u.departmentCode = :departmentCode " +
            "and u.provinceCode = :provinceCode " +
            "and u.districtCode = :districtCode " +
            "and u.active = 1 order by u.zipCode")
    List<KeyValuePair<Integer,String>> findZipCodeByCountryCodeAndDepartmentCodeAndProvinceCodeAndDistrictCode(@Param("countryCode")String countryCode,@Param("departmentCode")String departmentCode,@Param("provinceCode")String provinceCode,@Param("districtCode")String districtCode);
}
