package pe.compralo.parameters.api.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "pe.compralo.parameters")
@EntityScan("pe.compralo.parameters.application.domain")
@EnableJpaRepositories("pe.compralo.parameters.repository.interfaces")
public class MsparametersApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsparametersApplication.class, args);
	}

}
