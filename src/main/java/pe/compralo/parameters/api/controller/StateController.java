package pe.compralo.parameters.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.parameters.api.response.PaginationResponse;
import pe.compralo.parameters.api.query.StateQuery;
import pe.compralo.parameters.api.request.StateRequest;
import pe.compralo.parameters.api.response.StateResponse;
import pe.compralo.parameters.application.domain.State;
import pe.compralo.parameters.application.mapper.StateMapper;
import pe.compralo.parameters.service.interfaces.StateService;

@RestController
@RequestMapping("/states")
public class StateController {

	@Autowired
	private StateService stateService;

	@GetMapping("/{id}")
	public ResponseEntity<StateResponse> getSelect(@PathVariable int id) {
		State o = stateService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<StateResponse>(StateMapper.ToSelectStateResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search/entity")
	public ResponseEntity<List<StateResponse>> getSelect(StateQuery q) {
		List<State> l = stateService.getSelectByEntity(q.getEntityId());

		return new ResponseEntity<List<StateResponse>>(StateMapper.ToArraySelectStateResponse(l),HttpStatus.OK);
	}


	@PostMapping()
	public ResponseEntity<StateResponse> setRegister(@RequestBody StateRequest r) {
		State o = StateMapper.FromStateRequest(r);

		stateService.setRegister(o);

		return new ResponseEntity<StateResponse>(StateMapper.ToRegisterStateResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<StateResponse> setUpdate(@PathVariable int id,@RequestBody StateRequest r) {
		State o = StateMapper.FromStateRequest(r);

		stateService.setUpdate(id,o);

		return new ResponseEntity<StateResponse>(StateMapper.ToRegisterStateResponse(o),HttpStatus.OK);
	}

}