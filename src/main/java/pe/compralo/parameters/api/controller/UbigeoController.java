package pe.compralo.parameters.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.parameters.api.query.UbigeoQuery;
import pe.compralo.parameters.api.response.UbigeoResponse;
import pe.compralo.parameters.application.domain.Ubigeo;
import pe.compralo.parameters.application.mapper.UbigeoMapper;
import pe.compralo.parameters.cross.utils.KeyValuePair;
import pe.compralo.parameters.service.interfaces.UbigeoService;

@RestController
@RequestMapping("/ubigeo")
public class UbigeoController {

	@Autowired
	private UbigeoService ubigeoService;

	@GetMapping("/{id}")
	public ResponseEntity<UbigeoResponse> getSelect(@PathVariable int id) {
		Ubigeo o = ubigeoService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<UbigeoResponse>(UbigeoMapper.ToSelectUbigeoResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search/country")
	public ResponseEntity<List<KeyValuePair<String,String>>> getSelectCountry() {
		List<KeyValuePair<String,String>> l = ubigeoService.getSelectCountry();

		return new ResponseEntity<List<KeyValuePair<String,String>>>(l,HttpStatus.OK);
	}

	@GetMapping("/search/department")
	public ResponseEntity<List<KeyValuePair<String,String>>> getSelectDepartment(UbigeoQuery q) {
		List<KeyValuePair<String,String>> l = ubigeoService.getSelectDepartment(q.getCountryCode());

		return new ResponseEntity<List<KeyValuePair<String,String>>>(l,HttpStatus.OK);
	}	

	@GetMapping("/search/province")
	public ResponseEntity<List<KeyValuePair<String,String>>> getSelectProvince(UbigeoQuery q) {
		List<KeyValuePair<String,String>> l = ubigeoService.getSelectProvince(q.getCountryCode(),q.getDepartmentCode());

		return new ResponseEntity<List<KeyValuePair<String,String>>>(l,HttpStatus.OK);
	}	

	@GetMapping("/search/district")
	public ResponseEntity<List<KeyValuePair<String,String>>> getSelectDistrict(UbigeoQuery q) {
		List<KeyValuePair<String,String>> l = ubigeoService.getSelectDistrict(q.getCountryCode(),q.getDepartmentCode(),q.getProvinceCode());

		return new ResponseEntity<List<KeyValuePair<String,String>>>(l,HttpStatus.OK);
	}	
	
	@GetMapping("/search/zip-code")
	public ResponseEntity<List<KeyValuePair<Integer,String>>> getSelectZipCode(UbigeoQuery q) {
		List<KeyValuePair<Integer,String>> l = ubigeoService.getSelectZipCode(q.getCountryCode(),q.getDepartmentCode(),q.getProvinceCode(),q.getDistrictCode());

		return new ResponseEntity<List<KeyValuePair<Integer,String>>>(l,HttpStatus.OK);
	}

}