package pe.compralo.parameters.api.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class StateRequest implements Serializable
{

	private int id;
	private int entityId;
	private String title;
	private String details;
	private int orderIndex;
	private Boolean active;

}