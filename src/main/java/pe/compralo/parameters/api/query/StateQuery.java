package pe.compralo.parameters.api.query;

import lombok.Data;

import java.util.Date;

@Data
public class StateQuery extends PaginationQuery
{

	private int id;
	private int entityId;
	private Boolean active;

}