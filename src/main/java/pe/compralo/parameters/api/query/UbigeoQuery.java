package pe.compralo.parameters.api.query;


import lombok.Data;

@Data
public class UbigeoQuery extends PaginationQuery
{

	private int id;
	private String countryCode;
	private String departmentCode;
	private String provinceCode;
	private String districtCode;
	private String zipCode;

}