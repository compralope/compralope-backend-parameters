package pe.compralo.parameters.api.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class StateResponse implements Serializable
{

	private int id;
	private int entityId;
	private String title;
	private String details;
	private int orderIndex;
	private Boolean active;

}