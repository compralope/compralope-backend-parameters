package pe.compralo.parameters.api.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class UbigeoResponse implements Serializable
{
	private int id;
	private String countryCode;
	private String countryTitle;
	private String departmentCode;
	private String departmentTitle;
	private String provinceCode;
	private String provinceTitle;
	private String districtCode;
	private String districtTitle;
	private String zipCode;
	private Boolean active;
}