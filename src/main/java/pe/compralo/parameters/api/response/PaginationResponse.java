package pe.compralo.parameters.api.response;

import java.util.List;

import lombok.Data;
import pe.compralo.parameters.application.domain.Pagination;

@Data
public class PaginationResponse<T> 
{
    public PaginationResponse(Pagination pagination, List<T> items)
    {
        this.pagination = pagination;
        this.items = items;
    }

    private Pagination pagination;
    private List<T> items;
}
