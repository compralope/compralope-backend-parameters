package pe.compralo.parameters.service.implementation;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.parameters.cross.utils.KeyValuePair;
import pe.compralo.parameters.repository.interfaces.UbigeoRepository;
import pe.compralo.parameters.service.interfaces.UbigeoService;
import pe.compralo.parameters.application.domain.Ubigeo;
import pe.compralo.parameters.application.domain.Pagination;

@Service
public class UbigeoServiceImp implements UbigeoService {

	@Autowired
	private UbigeoRepository ubigeoRepository;

	@Override
	public Ubigeo getSelect(int id) {
		Optional<Ubigeo> o = ubigeoRepository.findById(id);

		return (o.isPresent() ? o.get() : null);
	}

	@Override
	public List<KeyValuePair<String,String>> getSelectCountry() {
		return ubigeoRepository.findCountry();
	}

	@Override
	public List<KeyValuePair<String,String>> getSelectDepartment(String countryCode) {
		return ubigeoRepository.findDepartmentByCountryCode(countryCode);
	}
	
	@Override
	public List<KeyValuePair<String,String>> getSelectProvince(String countryCode, String departmentCode) {
		return ubigeoRepository.findProvinceByCountryCodeAndDepartmentCode(countryCode,departmentCode);
	}

	@Override
	public List<KeyValuePair<String,String>> getSelectDistrict(String countryCode, String departmentCode, String provinceCode) {
		return ubigeoRepository.findDistrictByCountryCodeAndDepartmentCodeAndProvinceCode(countryCode,departmentCode,provinceCode);
	}

	@Override
	public List<KeyValuePair<Integer,String>> getSelectZipCode(String countryCode, String departmentCode, String provinceCode,String districtCode) {
		return ubigeoRepository.findZipCodeByCountryCodeAndDepartmentCodeAndProvinceCodeAndDistrictCode(countryCode,departmentCode,provinceCode,districtCode);
	}



}