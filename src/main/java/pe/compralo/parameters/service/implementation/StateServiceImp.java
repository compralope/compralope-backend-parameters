package pe.compralo.parameters.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.parameters.repository.interfaces.StateRepository;
import pe.compralo.parameters.service.interfaces.StateService;
import pe.compralo.parameters.application.domain.State;

@Service
public class StateServiceImp implements StateService {
	@Autowired
	private StateRepository stateRepository;

	@Override
	public State getSelect(int id) {
		Optional<State> o = stateRepository.findById(id);

		return (o.isPresent() ? o.get() : null);
	}

	public List<State> getSelectByEntity(int entityId) {
		return stateRepository.findByEntityIdAndActiveOrderByOrderIndexAsc(entityId,true);
	}

	public void setRegister(State o) {
		stateRepository.save(o);
	}

	public void setUpdate(int id,State o) {
		o.setId(id);

		stateRepository.save(o);
	}

}