package pe.compralo.parameters.service.interfaces;

import java.util.List;

import pe.compralo.parameters.application.domain.Ubigeo;
import pe.compralo.parameters.application.domain.Pagination;
import pe.compralo.parameters.cross.utils.KeyValuePair;

public interface UbigeoService {

	Ubigeo getSelect(int id);

	List<KeyValuePair<String,String>> getSelectCountry();

	List<KeyValuePair<String,String>> getSelectDepartment(String countryCode);

	List<KeyValuePair<String,String>> getSelectProvince(String countryCode, String departmentCode);

	List<KeyValuePair<String,String>> getSelectDistrict(String countryCode, String departmentCode, String provinceCode);

	List<KeyValuePair<Integer,String>> getSelectZipCode(String countryCode, String departmentCode, String provinceCode,String districtCode);

}