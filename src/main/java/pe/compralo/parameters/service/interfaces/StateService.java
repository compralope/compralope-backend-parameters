package pe.compralo.parameters.service.interfaces;

import java.util.List;

import pe.compralo.parameters.application.domain.State;
import pe.compralo.parameters.application.domain.Pagination;

public interface StateService {

	State getSelect(int id);

	List<State> getSelectByEntity(int entityId);

	void setRegister(State o);

	void setUpdate(int id,State o);

}