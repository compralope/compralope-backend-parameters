FROM openjdk:8-alpine
ADD target/msparameters-1.0.jar /usr/share/msparameters-1.0.jar
EXPOSE  8086
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/msparameters-1.0.jar"]